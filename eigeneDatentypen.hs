{-# LANGUAGE TemplateHaskell #-}
module EigeneDatentypen where


--Tokeneiser
data Token = KlammerAuf | KlammerZu | Implikation | Punkt | Komma | Not | Variable String | Name String  | Unknown String | Fehler String deriving Show

instance Eq Token where
    (==) (Variable _) (Variable _) = True
    (==) (Name _) (Name _) = True
    KlammerAuf == KlammerAuf = True
    KlammerZu == KlammerZu = True
    Implikation == Implikation = True
    Punkt == Punkt = True 
    Komma == Komma = True
    Not == Not = True
    (Fehler _) == (Fehler _) = True
    (==) _ _ = False

--Parser
data Elemente = Programm | Programmklausel | Ziel | Literal | NVLT | LTerm deriving Show

--Abstrakte Maschine
data MLBefehle = Push String | Unify String | Call | Return String | Backtrack | Prompt deriving Show

instance Eq MLBefehle where
    (==) (Push _) (Push _) = True
    (==) (Unify _) (Unify _) = True
    Call == Call = True
    (Return "pos") == (Return "pos") = True
    (Return "neg") == (Return "neg") = True
    Backtrack == Backtrack = True
    Prompt == Prompt = True
    (==) _ _ = False

data IntNil = Nil | Zahl Int deriving (Show, Eq)
data Storage = Storage [String] [String] [String] [String] [String] deriving Show
data Register = Register String IntNil Int IntNil IntNil Bool deriving Show
data AbstrakteMaschine = AbstrakteMaschine 
 { _codeStorage :: [MLBefehle]
 , _envStorage :: [String]
 , _stackStorage :: [String]
 , _uniStorage :: [String]
 , _trailStorage :: [String]
 , _instruction :: String
 , _programcounter :: IntNil
 , _topofthestack :: Int
 , _choicepoint :: IntNil -- Skript: last choice point -> choicepoint
 , _returnreg :: IntNil -- Skript: return register -> returnreg
 , _localenvironement :: IntNil -- Skript: local environement register -> local environement
 , _unificationpointer :: Int
 , _topofus :: Int
 , _topoftrail :: Int
 , _backtrackflag :: Bool
 , _pushcounter :: Int
 , _skipcounter :: Int
 , _argumentcounter :: IntNil
 } deriving Show


--Ast
--data Syntaxbaum = Ziel [String] [FaktundRegel]  deriving Show
data FaktundRegel = Regel String [String] | Fakt String deriving Show

--Ersetzt isUpper und isLower aus Data.List um den import zu sparen.
isupper :: [Char] -> Bool
isupper (a:_) = a `elem` upper
isupper _ = False

islower :: [Char] -> Bool
islower (a:_) = a `elem` lower
islower _ = False

--Die Listen für isupper und islower.
lower :: [Char]
lower = ['a' .. 'z']

upper :: [Char]
upper = ['A' .. 'Z']