{-# LANGUAGE TemplateHaskell #-}
module AbstrakteMaschine where
import Control.Lens
import EigeneDatentypen

start = AbstrakteMaschine 
 { _codeStorage = []
 , _envStorage = []
 , _stackStorage = []
 , _uniStorage = []
 , _trailStorage = []
 , _instruction = []
 , _programcounter = Zahl 0
 , _topofthestack = 0
 , _choicepoint = Nil
 , _returnreg = Nil
 , _localenvironement = Nil
 , _unificationpointer = 0
 , _topofus = 0
 , _topoftrail = 0
 , _backtrackflag = False
 , _pushcounter = 0
 , _skipcounter = 0
 , _argumentcounter = Nil
 }

makeLenses ''AbstrakteMaschine


--Change functions for registers
changeBacktrackFlag :: AbstrakteMaschine -> AbstrakteMaschine
changeBacktrackFlag maschine = over backtrackflag (not) maschine

changeReturnRegister :: AbstrakteMaschine -> IntNil -> AbstrakteMaschine
changeReturnRegister maschine newReturnRegister = set returnreg newReturnRegister maschine

changeProgramCounter :: AbstrakteMaschine -> IntNil  -> AbstrakteMaschine
changeProgramCounter maschine newProgramCounter = set programcounter newProgramCounter maschine

changeChoicePointer :: AbstrakteMaschine -> IntNil -> AbstrakteMaschine
changeChoicePointer maschine newChoicePointer  = set choicepoint newChoicePointer maschine

changeTopOfTheStack :: AbstrakteMaschine -> Int -> AbstrakteMaschine
changeTopOfTheStack maschine newTopOfTheStack  = set topofthestack newTopOfTheStack maschine


entpackeZahl :: IntNil -> Int
entpackeZahl (Zahl a) = a
entpackeZahl Nil = error "Can't unpack because value is Nil"


entpackeMLBefehl :: MLBefehle -> String
entpackeMLBefehl (Unify a) = a
entpackeMLBefehl (Push a) = a
entpackeMLBefehl (Return a) = a
entpackeMLBefehl _ = error "No value to unpack"


addZahl :: Int -> IntNil -> IntNil
addZahl a (Zahl b) = Zahl (a + b)
addZahl _ _ = error "Adding with Nil is not allowed"


lesen :: [Char] -> [Char]
lesen (a:_:_:_:_:as) | a == 'p' = as
lesen (a:_:_:_:_:_:as) | a == 'u' = as
lesen (z:_:_:_:_:as) | z == 'Z' =as
lesen a | otherwise = error ("Falsche eingabe: " ++ a)

writeNilorZahl :: IntNil -> [Char]
writeNilorZahl a = if a == Nil then "Nil" else show (entpackeZahl a)

readNilorZahl a = if a == "Nil" then Nil else Zahl (read (a)::Int)

find :: Int -> Int -> [a] -> a
find groß klein a = findf (groß-klein) a

findf :: Int -> [a] -> a
findf 0 (b:bs) = b
findf a (b:bs) = findf (a-1) bs
findf a bs = error ("Liste ist Leer aber a ist noch: " ++ (show a))

findStack :: Int -> AbstrakteMaschine -> String
findStack adresse eingabe = find (_topofthestack eingabe) adresse (_stackStorage eingabe)

finduni adresse eingabe = find (_topofus eingabe) adresse (_uniStorage eingabe)

--c Befhele
c_last :: AbstrakteMaschine -> Int
c_last eingabe = length (view codeStorage eingabe) - 1


c_first :: AbstrakteMaschine -> IntNil
c_first eingabe = if c_goal eingabe /= 0 then Zahl 0 else Nil


c_goal :: AbstrakteMaschine -> Int
c_goal eingabe = hilfe 0 0 (_codeStorage eingabe) where
    hilfe z1 z2 codeStorage | findf z2 codeStorage == Prompt = z1
    hilfe z1 z2 codeStorage | findf z2 codeStorage == (Return "pos") = hilfe (z2 + 1) (z2 + 1) codeStorage
    hilfe z1 z2 codeStorage | otherwise = hilfe z1 (z2 + 1) codeStorage


c_next :: Int -> AbstrakteMaschine -> IntNil
c_next zahl eingabe = hilfe (Zahl zahl) eingabe where
    hilfe zahl eingabe | entpackeZahl zahl == c_last eingabe = Nil
    hilfe zahl eingabe | (findf (entpackeZahl zahl) (_codeStorage eingabe)) == (Return "pos") && (entpackeZahl zahl + 1) /= c_goal eingabe = addZahl 1 zahl
    hilfe zahl eingabe = hilfe (addZahl 1 zahl) eingabe

s_add :: String -> String -> AbstrakteMaschine -> IntNil
s_add symbol mode eingabe = hilfe symbol mode Nil 0 eingabe where --intialisierung von i unsicher
    hilfe symbol "unify" add i eingabe = s_addwhile symbol add (localenvironementINT eingabe) eingabe
    hilfe symbol "push" add i eingabe  = if (_choicepoint eingabe) == Nil then s_addwhile symbol Nil i eingabe else s_addwhile symbol add  (read (stackCnum 3 eingabe)::Int) eingabe

s_addwhile :: String -> IntNil -> Int -> AbstrakteMaschine -> IntNil
s_addwhile symbol add i eingabe | notElem "EndEnv" (_stackStorage eingabe) = add
s_addwhile symbol add i eingabe | (findStack i eingabe) == "EndEnv" = add
s_addwhile symbol add i eingabe | add /= Nil = add
s_addwhile symbol add i eingabe | symbol == (findSymbol (findStack i eingabe)) = (s_addwhile symbol (Zahl i) (i + 1) eingabe) -- Changed so wenn gleichnamige Variable gefunden wird wird eine Bindung auf diese stack stelle gesetzt  (nach skript 148)
s_addwhile symbol add i eingabe | otherwise = (s_addwhile symbol add (i + 1) eingabe)



deref add eingabe = if add == Nil then Nil else if head (findStack (entpackeZahl add) eingabe) == 'S' then add else derefif add (varAdresse (findStack (entpackeZahl add) eingabe)) eingabe

derefif :: IntNil -> IntNil -> AbstrakteMaschine -> IntNil
derefif add add2 eingabe = if add2 == Nil then add else deref add2 eingabe


display eingabe = hilfe 1 eingabe where
    hilfe i eingabe | findStack i eingabe /= "EndEnv" = do
        display_term (entpackeZahl(deref (Zahl i) eingabe)) eingabe
        putStrLn ( " / " ++ (findSymbol (findStack i eingabe)))
        hilfe (i +1) eingabe
    hilfe i eingabe | findStack i eingabe == "EndEnv" = putStr ""

display_term :: Int -> AbstrakteMaschine -> IO()
display_term add eingabe | head (findStack (entpackeZahl (deref (Zahl add) eingabe)) eingabe) == 'V' && varAdresse(findStack (entpackeZahl (deref (Zahl add) eingabe)) eingabe) == Nil && (_trailStorage eingabe) == [] = putStr $ findSymbol (findStack (entpackeZahl (deref (Zahl add) eingabe)) eingabe)
display_term add eingabe | head (findStack (entpackeZahl (deref (Zahl add) eingabe)) eingabe) == 'V' && varAdresse(findStack (entpackeZahl (deref (Zahl add) eingabe)) eingabe) == Nil = display_term (read (head (_trailStorage eingabe))::Int) eingabe
display_term add eingabe | head (findStack (entpackeZahl (deref (Zahl add) eingabe)) eingabe) == 'S' && strArity (findStack (entpackeZahl (deref (Zahl add) eingabe)) eingabe) == 0 = putStr $ findSymbol (findStack (entpackeZahl (deref (Zahl add) eingabe)) eingabe)
display_term add eingabe | head (findStack (entpackeZahl (deref (Zahl add) eingabe)) eingabe) == 'S' = do
    putStr $ findSymbol (findStack (entpackeZahl (deref (Zahl add) eingabe)) eingabe) ++ "("
    display_term ((entpackeZahl (deref (Zahl (add)) eingabe )+ 1)) eingabe 
    putStr (")")


add_AC n eingabe = if (_argumentcounter eingabe) /= Nil then setargumentcounter (entpackeZahl (_argumentcounter eingabe) + n) eingabe else eingabe

restore_AC_UP eingabe | (_argumentcounter eingabe) == Nil = eingabe
restore_AC_UP eingabe = if (entpackeZahl (_argumentcounter eingabe)) == 0 then physischesentkellern $ settopofus (_topofus eingabe - 2) $ setunificationpointer (_topofus eingabe) $ setargumentcounter (_topofus eingabe - 1) eingabe else eingabe --überprüfung das AC nicht nil ist

save_AC_UP eingabe = if (_unificationpointer eingabe) <= (read (stackCnum 5 eingabe)::Int) && entpackeZahl (deref (Zahl (_unificationpointer eingabe)) eingabe) /= (_unificationpointer eingabe)  && 0 /= arity (findStack (entpackeZahl (deref (Zahl (_unificationpointer eingabe)) eingabe)) eingabe) eingabe
    then setargumentcounter 0 $ setunificationpointer (entpackeZahl (deref (Zahl (_unificationpointer eingabe)) eingabe)) $ adduniStorage ((show (_unificationpointer eingabe + 1)):[writeNilorZahl (_argumentcounter eingabe)]) eingabe
    else eingabe

arity a eingabe = if head a == 'V' then 0 else if head a == 'S' then strArity a else error "arity aufgerufen ohne das es auf eine Variable oder Funktion zeigt"

vereinigen add1 add2 eingabe = vereinigenwhile [add2, add1] eingabe 

vereinigenwhile :: [Int] -> AbstrakteMaschine -> Bool
vereinigenwhile (d1:d2:list) eingabe | (length list > 0) = vereinigenif d1 d2 list eingabe
vereinigenwhile _ _ = True


vereinigenif :: Int -> Int -> [Int] -> AbstrakteMaschine -> Bool
vereinigenif d1 d2 list eingabe = if d1 == d2
    then vereinigenwhile list eingabe
    else if head (findStack d1 eingabe) == 'V' && varAdresse (findStack d1 eingabe) == Nil
        then vereinigenwhile list (addtrailStorage [show d1] $ changestackelem d1 ("VAR " ++ findSymbol (findStack d1 eingabe) ++ " " ++ show d2) eingabe)
        else if head (findStack d2 eingabe) == 'V' && varAdresse (findStack d2 eingabe) == Nil
            then vereinigenwhile list $ addtrailStorage [show d2] $ changestackelem d2 ("VAR " ++ findSymbol (findStack d2 eingabe) ++ " " ++ show d1) eingabe
            else if (findSymbol (findStack d1 eingabe) /= findSymbol (findStack d2 eingabe)) || (strArity (findStack d1 eingabe) /= strArity (findStack d2 eingabe))
                then False
                else vereinigenfor 1 d1 d2 list eingabe

vereinigenfor zaehler d1 d2 list eingabe | strArity (findStack d1 eingabe) <= zaehler = vereinigenfor (zaehler + 1) d1 d2 ((d2 + 1):((d1 + 1):list)) eingabe 
vereinigenfor _ d1 d2 list eingabe = vereinigenwhile list eingabe

emptytrail eingabe | (_trailStorage eingabe) == [] = settopoftrail 0 eingabe
emptytrail eingabe = let readTrailStorage = (read (head (_trailStorage eingabe))::Int) in emptytrail $ overtrailStorage tail $ changestackelem readTrailStorage ("VAR " ++ findSymbol (findStack readTrailStorage eingabe) ++ " Nil") eingabe


programcounterINT :: AbstrakteMaschine -> Int
programcounterINT eingabe = entpackeZahl $ view programcounter eingabe


setargumentcounter zahl eingabe = set argumentcounter (Zahl zahl) eingabe

setargumentcounterNil eingabe = set argumentcounter Nil eingabe


setpushcounter zahl eingabe = set pushcounter zahl eingabe

overstackStorage befehl eingabe = over stackStorage befehl eingabe


settopofus zahl eingabe = set topofus zahl eingabe

choicepointINT :: AbstrakteMaschine -> Int
choicepointINT eingabe = entpackeZahl (view choicepoint eingabe)


setcodeStorage :: [MLBefehle] -> AbstrakteMaschine -> AbstrakteMaschine
setcodeStorage liste eingabe = set codeStorage liste eingabe


setprogramcounter :: Int -> AbstrakteMaschine -> AbstrakteMaschine
setprogramcounter zahl eingabe = set programcounter (Zahl (zahl)) eingabe


setprogramcounterNil :: AbstrakteMaschine -> AbstrakteMaschine
setprogramcounterNil eingabe = set programcounter Nil eingabe


incprogramcounter1 :: AbstrakteMaschine -> AbstrakteMaschine
incprogramcounter1 eingabe = set programcounter (addZahl 1 (_programcounter eingabe)) eingabe


settopofthestack :: Int -> AbstrakteMaschine -> AbstrakteMaschine
settopofthestack zahl eingabe = set topofthestack zahl eingabe

settopoftrail zahl eingabe = set topoftrail zahl eingabe

localenvironementINT eingabe = entpackeZahl (view localenvironement eingabe)

setlocalenvironementNil eingabe = set localenvironement Nil eingabe

setlocalenvironement zahl eingabe = set localenvironement zahl eingabe

setchoicepoint :: Int -> AbstrakteMaschine -> AbstrakteMaschine
setchoicepoint zahl eingabe = set choicepoint (Zahl zahl) eingabe


setunificationpointer zahl eingabe = set unificationpointer zahl eingabe

setreturnreg :: Int -> AbstrakteMaschine -> AbstrakteMaschine
setreturnreg zahl eingabe = set returnreg (Zahl zahl) eingabe


addstackStorage :: AbstrakteMaschine -> String -> AbstrakteMaschine
addstackStorage eingabe element = set stackStorage (element:(_stackStorage eingabe)) eingabe

adduniStorage element eingabe = over topofus (+ length element) $ set uniStorage (element ++ (_uniStorage eingabe)) eingabe

addtrailStorage element eingabe = over topoftrail (+ length element) $ set trailStorage (element ++ (_trailStorage eingabe)) eingabe

overtrailStorage funktion eingabe = over trailStorage funktion eingabe

changestackStorage :: [String] -> AbstrakteMaschine -> AbstrakteMaschine
changestackStorage element eingabe = set stackStorage (element ++ (_stackStorage eingabe)) eingabe

stackUP eingabe = findStack (_unificationpointer eingabe) eingabe

setskipcounter zahl eingabe = set skipcounter zahl eingabe

stackR :: AbstrakteMaschine -> String
stackR eingabe = findStack (entpackeZahl (_returnreg eingabe)) eingabe 

stackRnum :: Int -> AbstrakteMaschine -> String
stackRnum zahl eingabe = findStack (entpackeZahl (_returnreg eingabe) + zahl) eingabe


stackRint :: AbstrakteMaschine -> Int
stackRint eingabe = read (stackR eingabe)::Int


stackRnil :: AbstrakteMaschine -> Bool
stackRnil eingabe = (stackR eingabe) == "Nil"

varAdresse element = hilfe (words element) where
    hilfe (_:_:e3:_) = if e3 == "Nil" then Nil else Zahl (read (e3)::Int)

strArity element = hilfe (words element) where
    hilfe (_:_:e3:_) = read (e3)::Int


findSymbol element = hilfe (words element) where
    hilfe (_:e2:_) = e2
    hilfe list = error ("findSymbol hat Liste: "++ show list ++ " uebergeben bekommen")

trailnum zahl eingabe = read (find (view topoftrail eingabe) zahl (view trailStorage eingabe))::Int

stackC :: AbstrakteMaschine -> String
stackC eingabe = find (view topofthestack eingabe) (entpackeZahl (view choicepoint eingabe)) (view stackStorage eingabe)

stackCnum :: Int -> AbstrakteMaschine -> String
stackCnum zahl eingabe = findStack (entpackeZahl (_choicepoint eingabe) + zahl) eingabe

stackCint :: AbstrakteMaschine -> Int
stackCint eingabe = read (stackC eingabe)::Int


stackCnil :: AbstrakteMaschine -> Bool
stackCnil eingabe = (stackC eingabe) == "Nil"


setBacktrackFlag :: Bool -> AbstrakteMaschine -> AbstrakteMaschine
setBacktrackFlag a eingabe = set backtrackflag a eingabe


codeP :: AbstrakteMaschine -> MLBefehle
codeP eingabe = findf (programcounterINT eingabe) (view codeStorage eingabe)


showIntNil :: IntNil -> String
showIntNil wert = if wert == Nil then "Nil" else show $ entpackeZahl wert


changestackelem :: (Eq t, Num t) => t -> String -> AbstrakteMaschine -> AbstrakteMaschine
changestackelem ort element eingabe = set stackStorage (changelist ort element (_stackStorage eingabe)) eingabe


changelist :: (Eq t, Num t) => t -> a -> [a] -> [a]
changelist ort element liste = hilfe (reverse liste) ort element []  where 
    hilfe (_:as) 1 element tail = (reverse as) ++ (element:tail)
    hilfe (a:as) ort element tail = hilfe as (ort - 1) element (a:tail)
    hilfe _ _ _ _ = error "listen verändern ist fehlgeschlagen"

physischesentkellern :: AbstrakteMaschine -> AbstrakteMaschine
physischesentkellern eingabe | length (_stackStorage eingabe) > (_topofthestack eingabe) =  physischesentkellern $ over stackStorage tail eingabe
physischesentkellern eingabe | length (_uniStorage eingabe) > (_topofus eingabe) =  physischesentkellern $ over uniStorage tail eingabe
physischesentkellern eingabe = eingabe

startbelegung :: [MLBefehle] -> AbstrakteMaschine
startbelegung eingabe = setprogramcounter (c_goal (setcodeStorage eingabe start)) $ setcodeStorage eingabe start 

{-
hilfe zahl eingabe (Zahl 1) where
    hilfe zahl eingabe erg = if zahl < 0 || erg == Nil then erg else hilfe (zahl - 1) eingabe (c_nexterg (c_nextlist (_codeStorage eingabe) (entpackeZahl erg)) erg)

c_nexterg (a:as) erg | a == Return && elem Return as = (addZahl 1 erg)
c_nexterg (a:as) _ | a == Prompt = Nil 
c_nexterg (_:as) erg = c_nexterg as (addZahl 1 erg)
c_nexterg [] _ = error "empty codeStack error in c_next"

c_nextlist list 0 = list
c_nextlist (_:list) zahl = c_nextlist list (zahl - 1)

push (AbstrakteMaschine (Register instruction (Zahl p) t _ (Zahl c) b) (Storage codeStorage envStorage stackStorage  uniStorage trailStorage)) = AbstrakteMaschine (Register ""  (Zahl (p+1)) (t+4) (Zahl (c+1)) (Zahl (t+1)) b) (Storage codeStorage envStorage (lesen(instruction):(show(p+3)):(show c):(show (c_first envStorage)):(show b):stackStorage) uniStorage trailStorage) 
--c muss eigt gesetzt sein, wenn r berechnet werden soll...
--push (Register instruction p t _ Nil b) (Storage codeStorage envStorage stackStorage  uniStorage trailStorage) = AbstrakteMaschine (Register ""  (p+1) (t+4) (Zahl (c+1)) (Zahl (t+1)) b) (Storage codeStorage envStorage (lesen(instruction):(show(p+3)):(show c):(show (c_first envStorage)):(show b):stackStorage) uniStorage trailStorage)
-}

push :: String -> AbstrakteMaschine -> AbstrakteMaschine
push a eingabe = incprogramcounter1 $ pushcase a eingabe

pushcase a eingabe | head a == 'S' = pushtopofthestack $ pushstack a eingabe
pushcase a eingabe | head a == 'V' = pushtopofthestack $ pushstack (a ++ " " ++ writeNilorZahl (s_add (findSymbol a) "push" eingabe)) eingabe
pushcase "not" eingabe = pushtopofthestack $ pushreturnreg $ pushchoicepoint $ pushstackneg eingabe
pushcase "CHP" eingabe = pushflagCHP $ pushtopofthestackCHP $ pushunificationpointerCHP $ pushreturnregCHP $ pushchoicepointCHP $ pushstackCHP eingabe
pushcase "EndAtom" eingabe = pushstackEndAtom eingabe
pushcase "BegEnv" eingabe = setlocalenvironementNil eingabe
pushcase "EndEnv" eingabe = hilfe  $ pushtopofthestack $ pushstack "EndEnv" eingabe where
    hilfe eingabe = setlocalenvironement (pushlocalenvironment (overstackStorage tail eingabe)) eingabe

pushlocalenvironment eingabe =  pushlocalenvironmentcount 0 eingabe 

pushlocalenvironmentcount n eingabe | (_stackStorage eingabe) /= [] = if head (head (_stackStorage eingabe)) == 'V'
    then pushlocalenvironmentcount (n + 1) $ overstackStorage tail eingabe
    else Zahl ((_topofthestack eingabe) - n)
pushlocalenvironmentcount n eingabe = Zahl ((_topofthestack eingabe) - n) 


-- a = "STR 'Symbol' 'Arity'"
pushstack :: String -> AbstrakteMaschine -> AbstrakteMaschine
pushstack a eingabe = changestackStorage [a] eingabe

pushstackneg :: AbstrakteMaschine -> AbstrakteMaschine
pushstackneg eingabe = settopofthestack (_topofthestack eingabe + 3) $ changestackStorage ("not":(showIntNil (addZahl 4 (_programcounter eingabe))):(showIntNil (_choicepoint eingabe)):["Nil"]) eingabe


pushstackCHP eingabe = changestackStorage ("":((show (_topoftrail eingabe)):((writeNilorZahl (_localenvironement eingabe)):("":(showIntNil (_choicepoint eingabe)):[writeNilorZahl (c_first eingabe)])))) eingabe


pushstackEndAtom eingabe = changestackelem (choicepointINT eingabe + 5) (show (_topofthestack eingabe)) $ changestackelem (choicepointINT eingabe + 2) (show (programcounterINT eingabe + 3)) eingabe 


pushchoicepoint :: AbstrakteMaschine -> AbstrakteMaschine
pushchoicepoint eingabe = setchoicepoint ((_topofthestack eingabe) + 1) eingabe

pushchoicepointCHP eingabe = setchoicepoint ((_topofthestack eingabe) + 1) eingabe

pushreturnreg :: AbstrakteMaschine -> AbstrakteMaschine
pushreturnreg eingabe = setreturnreg (choicepointINT eingabe + 1) eingabe


pushreturnregCHP eingabe = setreturnreg (choicepointINT eingabe + 1) eingabe


pushunificationpointerCHP eingabe = setunificationpointer (choicepointINT eingabe + 6) eingabe


pushtopofthestack :: AbstrakteMaschine -> AbstrakteMaschine
pushtopofthestack eingabe = settopofthestack ((_topofthestack eingabe) + 1) eingabe


pushtopofthestackCHP eingabe = settopofthestack ((_topofthestack eingabe) + 6) eingabe

pushflagCHP eingabe = setargumentcounterNil $ setpushcounter 0 $ settopofus 0 eingabe


unify :: String -> AbstrakteMaschine -> AbstrakteMaschine
unify a eingabe = incprogramcounter1 (if (_pushcounter eingabe) >= 1 then unifythen a eingabe else unifyelse a eingabe )

unifythen :: String -> AbstrakteMaschine -> AbstrakteMaschine
unifythen a eingabe | head a == 'S' = unifypush a (changestackStorage [a] eingabe)
unifythen a eingabe | head a == 'V' = unifypush (a ++ " " ++ writeNilorZahl (s_add (findSymbol a) "unify" eingabe)) eingabe

unifypush a eingabe = setpushcounter ((_pushcounter eingabe) - 1 + arity a eingabe) (settopofthestack ((_topofthestack eingabe) + 1) eingabe)

unifyelse a eingabe = if head a == 'V' then unifyVAR a eingabe else unifySTR a eingabe

unifyVAR a eingabe = if unifyVARbedingung a eingabe then unifyVARthen a eingabe else unifyVARelse a eingabe


unifyVARbedingung a eingabe = "Nil" == (head $ tail $ tail $ words $ findStack (entpackeZahl (deref (s_add (findSymbol a) "unify" eingabe) eingabe)) eingabe) -- && head (findStack (deref (entpackeZahl (s_add (findSymbol a) "unify" eingabe)) eingabe) eingabe) == 'V' -- durch localenvironment in s_add wird die prüfung auf var nicht gebraucht -- wenn s_add Nil ist dann ist die Variable ungebunden

unifyVARthen a eingabe = unifyVARwhile $ setskipcounter (arity (stackUP eingabe) eingabe) $ addtrailStorage [writeNilorZahl (deref (s_add (findSymbol a) "unify" eingabe) eingabe)] $ unifyVARchangestack a eingabe

unifyVARchangestack a eingabe = changestackelem (entpackeZahl (deref (s_add (findSymbol a) "unify" eingabe) eingabe)) ("VAR " ++ findSymbol (findStack (entpackeZahl(deref (s_add (findSymbol a) "unify" eingabe) eingabe)) eingabe) ++ " " ++ show (_unificationpointer eingabe)) eingabe

unifyVARelse a eingabe = unifyVARwhile $ unifyvereinigungzuruecksetzen $ setBacktrackFlag (vereinigen (entpackeZahl (deref (s_add (findSymbol a) "unify" eingabe) eingabe)) (_unificationpointer eingabe) eingabe) $ unifyvereinigungsichern eingabe

unifyvereinigungsichern eingabe = adduniStorage [show (_topofthestack eingabe)] eingabe

unifyvereinigungzuruecksetzen eingabe = physischesentkellern $ settopofus (_topofus eingabe -1) $ settopofthestack (read (head (_uniStorage eingabe))::Int) eingabe

unifyVARwhile eingabe | (_skipcounter eingabe) >= 1 = setskipcounter (_skipcounter eingabe -1 + arity (stackUP eingabe) eingabe) $ setunificationpointer (_unificationpointer eingabe + 1) eingabe
unifyVARwhile eingabe = unifyVARflags eingabe

unifyVARflags eingabe = setunificationpointer (_unificationpointer eingabe + 1) $ restore_AC_UP $ add_AC (-1) eingabe --anpassen

unifySTR a eingabe = if unifySTRbedingung eingabe then unifySTRthen a eingabe else unifySTRelse a eingabe

unifySTRbedingung eingabe = head (findStack (entpackeZahl (deref (Zahl (_unificationpointer eingabe)) eingabe)) eingabe) == 'V' && varAdresse (findStack (entpackeZahl(deref (Zahl (_unificationpointer eingabe)) eingabe)) eingabe) == Nil

unifySTRthen a eingabe = setunificationpointer (_unificationpointer eingabe +1) $ restore_AC_UP $ add_AC (-1) $ setargumentcounter (strArity a) $ settopofthestack (_topofthestack eingabe + 1) $ changestackStorage [a] $ changestackelem (entpackeZahl (deref (Zahl (_unificationpointer eingabe)) eingabe)) ("VAR " ++ findSymbol (findStack (entpackeZahl (deref (Zahl (_unificationpointer eingabe)) eingabe)) eingabe) ++ " " ++ show ((_topofthestack eingabe) + 1)) $ addtrailStorage [writeNilorZahl (deref (Zahl (_unificationpointer eingabe)) eingabe)]  eingabe



unifySTRelse a eingabe = if (findSymbol a /= findSymbol (findStack (entpackeZahl (deref (Zahl (_unificationpointer eingabe)) eingabe)) eingabe)) || (strArity a /= strArity (findStack (entpackeZahl (deref (Zahl (_unificationpointer eingabe)) eingabe)) eingabe)) 
    then setBacktrackFlag True eingabe 
    else save_AC_UP $ setunificationpointer (_unificationpointer eingabe +1) $ restore_AC_UP (if strArity a >= 1
        then add_AC (strArity a) eingabe
        else add_AC (-1) eingabe)


unifybacktrackflag :: String -> AbstrakteMaschine -> AbstrakteMaschine
unifybacktrackflag a eingabe = setBacktrackFlag ((find (_topofthestack eingabe) (entpackeZahl (addZahl 3 (_choicepoint eingabe))) (_stackStorage eingabe)) /= a) eingabe


call :: AbstrakteMaschine -> AbstrakteMaschine
call eingabe = if stackCnil eingabe then callthen eingabe else callelse eingabe


callthen :: AbstrakteMaschine -> AbstrakteMaschine
callthen eingabe = incprogramcounter1 (setBacktrackFlag True eingabe)


callelse :: AbstrakteMaschine -> AbstrakteMaschine
callelse eingabe = changestackelem (entpackeZahl (_choicepoint eingabe)) (writeNilorZahl $ c_next (stackCint eingabe) eingabe) $ setprogramcounter (stackCint eingabe) $ eingabe


returnL :: String -> AbstrakteMaschine -> AbstrakteMaschine
returnL "neg" eingabe = returnL "pos" $ setBacktrackFlag False $ changestackelem (read (stackRnum (-1) eingabe)::Int) "Nil" eingabe
returnL "pos" eingabe  | (read (stackRnum 1 eingabe)::Int) < c_goal eingabe && (findf (read (stackRnum 1 eingabe)::Int) (_codeStorage eingabe)) /= Prompt = returnLcounter $ if entpackeMLBefehl (findf (read (stackRnum 1 eingabe)::Int) (_codeStorage eingabe)) == "CHP" 
    then changestackelem (entpackeZahl (_returnreg eingabe) + 1) (findStack (stackRint eingabe + 1) eingabe) $ setprogramcounter (read (stackRnum 1 eingabe)::Int) eingabe
    else setprogramcounter (read (stackRnum 1 eingabe)::Int) eingabe
returnL "pos" eingabe = returnLcounter $ setprogramcounter (read (stackRnum 1 eingabe)::Int) eingabe
-- altes return pos: returnL "pos" eingabe = returnLcounter $ setprogramcounter (read (stackRnum 1 eingabe)::Int) eingabe


returnLcounter :: AbstrakteMaschine -> AbstrakteMaschine
returnLcounter eingabe = returnLif $ setlocalenvironement (Zahl (read (stackRnum 2 eingabe)::Int)) eingabe  --localenvironment kann nicht au Nil gesetzt werden.
--if not (stackRnil eingabe) then  else eingabe
returnLif eingabe = if (stackR eingabe) /= "Nil" then setreturnreg (stackRint eingabe + 1) eingabe else eingabe

returnLthen :: AbstrakteMaschine -> AbstrakteMaschine
returnLthen eingabe = setreturnreg ((stackRint eingabe) + 1) eingabe

backtrack :: AbstrakteMaschine -> AbstrakteMaschine
backtrack eingabe = if (_backtrackflag eingabe) then backtrackwhile eingabe else incprogramcounter1 eingabe 

backtrackwhile :: AbstrakteMaschine -> AbstrakteMaschine
backtrackwhile eingabe | backtrackwhilebedingung eingabe = backtrackwhile  (hilfe  (setchoicepoint (stackRint eingabe) eingabe)) where
    hilfe a = (setreturnreg (choicepointINT a + 1) a) 
backtrackwhile eingabe | otherwise = backtrackfor (1 + (read (stackCnum 4 eingabe)::Int)) (backtracksettingflags eingabe)

backtrackwhilebedingung :: AbstrakteMaschine -> Bool
backtrackwhilebedingung eingabe = stackCnil eingabe && not (stackRnil eingabe)


backtracksettingflags eingabe = setargumentcounterNil $ setpushcounter 0 $ settopofus 0 $ setunificationpointer (choicepointINT eingabe + 6) $ setlocalenvironement (Zahl (_topofthestack eingabe + 1)) $ physischesentkellern $ settopofthestack (read (stackCnum 5 eingabe)::Int) eingabe

backtrackfor :: Int -> AbstrakteMaschine -> AbstrakteMaschine
backtrackfor i eingabe | i > (_topoftrail eingabe) = backtrackif2 $ settopoftrail (read (stackCnum 4 eingabe)::Int) eingabe 
backtrackfor i eingabe | i <= (_topoftrail eingabe) = backtrackfor (i + 1) $ backtrackif i eingabe 

backtrackif :: Int -> AbstrakteMaschine -> AbstrakteMaschine
backtrackif i eingabe = if (trailnum i eingabe) <= (_topofthestack eingabe) then backtrackthen i eingabe else eingabe

backtrackthen i eingabe = if head (findStack (trailnum i eingabe) eingabe) /= 'V' then eingabe else changestackelem (trailnum i eingabe) ("VAR " ++ (findSymbol (findStack (trailnum i eingabe) eingabe)) ++ " Nil") eingabe

backtrackif2 :: AbstrakteMaschine -> AbstrakteMaschine
backtrackif2 eingabe = if stackC eingabe == "Nil" then setprogramcounter (c_last eingabe) eingabe else backtrackelse eingabe

backtrackelse :: AbstrakteMaschine -> AbstrakteMaschine
backtrackelse eingabe = setBacktrackFlag False $ changestackelem (entpackeZahl (_choicepoint eingabe)) (writeNilorZahl $ c_next (stackCint eingabe) eingabe) $ setprogramcounter (stackCint eingabe) eingabe


prompt :: AbstrakteMaschine -> IO ()
prompt eingabe = if _backtrackflag eingabe
    then do
        putStrLn "no (more) solutions"
        _ <- getLine
        putStr ""
    else do
        display eingabe
        putStrLn "more ?"
        x <- getLine
        if x == ";" 
            then do 
                promptmore eingabe
            else do 
                putStr ""


promptmore :: AbstrakteMaschine -> IO ()
promptmore eingabe = prompt $ abstrakteMaschine (emptytrail (setprogramcounter (entpackeZahl (_programcounter eingabe) - 1) (changeBacktrackFlag eingabe))) 


abstrakteMaschine :: AbstrakteMaschine -> AbstrakteMaschine
abstrakteMaschine eingabe | (codeP eingabe) == Prompt = eingabe
abstrakteMaschine eingabe | (codeP eingabe) == (Push "") = abstrakteMaschine2 (push (entpackeMLBefehl (codeP eingabe)) eingabe)
abstrakteMaschine eingabe | (codeP eingabe) == (Unify "") = abstrakteMaschine2 (unify (entpackeMLBefehl (codeP eingabe)) eingabe)
abstrakteMaschine eingabe | (codeP eingabe) == Call = abstrakteMaschine2 (call eingabe)
abstrakteMaschine eingabe | ((codeP eingabe) == (Return "pos")) || ((codeP eingabe) == (Return "neg")) = abstrakteMaschine2 (returnL (entpackeMLBefehl (codeP eingabe)) eingabe)
abstrakteMaschine eingabe | (codeP eingabe) == Backtrack = abstrakteMaschine2 (backtrack eingabe)

t :: (Eq a, Num a) => AbstrakteMaschine -> a -> AbstrakteMaschine
t x y = testabstrakteMaschine x y --entfernen

testabstrakteMaschine :: (Eq a, Num a) => AbstrakteMaschine -> a -> AbstrakteMaschine
testabstrakteMaschine eingabe 0 = eingabe
testabstrakteMaschine eingabe zahl | (codeP eingabe) == Prompt = eingabe
testabstrakteMaschine eingabe zahl | (codeP eingabe) == (Push "") = testabstrakteMaschine (push (entpackeMLBefehl (codeP eingabe)) eingabe) (zahl - 1)
testabstrakteMaschine eingabe zahl | (codeP eingabe) == (Unify "") = testabstrakteMaschine (unify (entpackeMLBefehl (codeP eingabe)) eingabe) (zahl - 1)
testabstrakteMaschine eingabe zahl | (codeP eingabe) == Call = testabstrakteMaschine (call eingabe) (zahl - 1)
testabstrakteMaschine eingabe zahl | ((codeP eingabe) == (Return "pos")) || ((codeP eingabe) == (Return "neg")) = testabstrakteMaschine (returnL (entpackeMLBefehl (codeP eingabe)) eingabe) (zahl - 1)
testabstrakteMaschine eingabe zahl | (codeP eingabe) == Backtrack = testabstrakteMaschine (backtrack eingabe) (zahl - 1)


abstrakteMaschine2 :: AbstrakteMaschine -> AbstrakteMaschine
abstrakteMaschine2 eingabe | (codeP eingabe) == Prompt = eingabe
abstrakteMaschine2 eingabe | (codeP eingabe) == (Push "") = do
    let erg = (push (entpackeMLBefehl (codeP eingabe)) eingabe)
    abstrakteMaschine2 erg
abstrakteMaschine2 eingabe | (codeP eingabe) == (Unify "") = do
    let erg = unify (entpackeMLBefehl (codeP eingabe)) eingabe
    abstrakteMaschine2 erg
abstrakteMaschine2 eingabe | (codeP eingabe) == Call = do
    let erg = abstrakteMaschine2 (call eingabe)
    abstrakteMaschine2 erg
abstrakteMaschine2 eingabe | ((codeP eingabe) == (Return "pos")) || ((codeP eingabe) == (Return "neg")) = do
    let erg = abstrakteMaschine2 (returnL (entpackeMLBefehl (codeP eingabe)) eingabe)
    abstrakteMaschine2 erg
abstrakteMaschine2 eingabe | (codeP eingabe) == Backtrack = do
    let erg = abstrakteMaschine2 (backtrack eingabe)
    abstrakteMaschine2 erg


test :: (Eq t, Num t) => AbstrakteMaschine -> t -> IO ()
test u n = testprint 1 $ reverse $ testhilfe u n

testprint :: (Show a1, Show a2, Num a1) => a1 -> [a2] -> IO ()
testprint n (a:b:as) = do
    putStrLn ("\nZeile " ++ show n ++  ": \n" ++ show a)
    testprint (n+1) (b:as)
testprint n (a:as) = do
    putStrLn ("\nZeile " ++ show n ++ ": \n" ++ show a)

testhilfe:: (Eq t, Num t) => AbstrakteMaschine -> t -> [AbstrakteMaschine]
testhilfe u 1 = [t u 1]
testhilfe u n = [t u n] ++ testhilfe u (n-1)

printbefehle liste = hilfe liste 0 where
    hilfe (l:l2:liste) zaehler | l == Return "pos" = do
        putStrLn ( "c" ++ show zaehler ++ ": " ++ (show l) ++ "\n")
        hilfe (l2:liste) (zaehler + 1)
    hilfe (l:l2:liste) zaehler = do
        putStrLn ( "c" ++ show zaehler ++ ": " ++ (show l))
        hilfe (l2:liste) (zaehler + 1)
    hilfe (l:liste) zaehler = putStrLn ( "c" ++ show zaehler ++ ": " ++ (show l))
{-

backtrack eingabe = if _backtrackflag eingabe then backtrackwhile eingabe else incprogramcounter1 eingabe

backtrackwhile eingabe | stackCnil eingabe && not (stackRnil eingabe) = settopofthestack  (setreturnreg (setchoicepoint (stackRint eingabe) eingabe) $ choicepointINT (setchoicepoint (stackRint eingabe) eingabe) + 1) $ choicepointINT (setchoicepoint (stackRint eingabe) eingabe) + 3
backtrackwhile eingabe | otherwise = if stackCnil eingabe then setprogramcounter eingabe (c_last eingabe) else backtrackelse eingabe

backtrackelse eingabe = setBacktrackFlag False $ changestackelem (stackCint eingabe) (show (c_next (stackCint eingabe) eingabe )) $ setprogramcounter eingabe (stackCint eingabe)

-}
