{-# LANGUAGE TemplateHaskell #-}
import System.IO
import EigeneDatentypen
import AbstrakteMaschine
import Parser
import Tokeniser
import Translate

main = do
    handle <- openFile "l.txt" ReadMode
    eingabe <- hGetContents handle
    let zeilenAufteilung = eingabe
    let tokens = tokenisingfun2 zeilenAufteilung []
    let parsed = programm tokens
    if head parsed == Fehler "" then putStr( entpackeFehler (head parsed)) else prompt $ abstrakteMaschine $ startbelegung $ translate parsed







