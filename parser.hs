module Parser where

import EigeneDatentypen

{-
Programm ::= { Programmklausel } Ziel .
Programmklausel ::= NichtVariableLTerm ( "." | Ziel ) .
Ziel ::= ":-" Literal { "," Literal } "." .
Literal ::= [ "not" ] LTerm .
NichtVariableLTerm ::= Name [ "(" LTerm {"," LTerm} ")" ] .
LTerm ::= Variable | NichtVariableLTerm .
-}

first:: Elemente -> [Token]
first Programm = [Name "", Implikation]
first Programmklausel = [Name ""]
first Ziel = [Implikation]
first Literal = [Not, Variable "", Name ""]
first NVLT = [Name ""]
first LTerm = [Variable "", Name ""]


entpackeFehler :: Token -> String
entpackeFehler (Fehler a) = a
entpackeFehler a | otherwise = error ("EntpackeFehler mit: " ++ fehlerUebersetzen a ++ " Aufgerufen.")


{-
void Programm(){
    (while (sym â first(Programmklauseln):
        code fÃ¼r Programmklauseln)
    code fÃ¼r Ziel
}
-}

programm :: [Token] -> [Token]
programm tokens = hilfe tokens tokens where
    hilfe [] _ = [Fehler ("Erhaltene Eingabe:\nErwartete Eingabe: NichtVariableLTerm (der mit Kleinbuchstaben beginnt) oder\":-\"")]
    hilfe ((Fehler x):xs) erg = ((Fehler x):xs)
    hilfe (t:tokens) erg | t == Implikation && ziel (t:tokens) == [] = erg
    hilfe (t:tokens) erg | t == Name "" = hilfe (programmklauseln (t:tokens)) erg
    hilfe (t:tokens) erg | otherwise = [Fehler ("Erhaltene Eingabe: " ++ fehlerUebersetzen t ++ "\nErwartete Eingaben: NichtVariableLTerm (der mit Kleinbuchstaben beginnt) oder \":-\"")]

{-
void Programmklauseln(){
    code fÃ¼r NVLT
    case 
    (sym â first(Ziel)
    code fÃ¼r Ziel);
    if (sym == ".") sym := nextSymbol();
    else error(...)
}
-}
programmklauseln :: [Token] -> [Token]
programmklauseln tokens = case (nVLT tokens) of 
    []                           -> [Fehler ("Erhaltene Eingabe:\nErwartete Eingaben: \".\" oder \":-\"")]
    ((Fehler x):xs)              -> ((Fehler x):xs)
    (x:xs) | elem x (first Ziel) -> ziel (x:xs)
    ((Punkt):xs)                 -> xs
    (x:xs) | otherwise           -> [Fehler ("Erhaltene Eingabe: " ++ fehlerUebersetzen x ++ "\nErwartete Eingaben: \".\" oder \":-\"")]

{-
void Ziel(){
    if (sym == ":-") sym := nextSymbol()
    else error(...)
    code fÃ¼r Literal
    while (sym==",") {sym:=nextSymbol
    code fÃ¼r Literal}
    if (sym == ".") sym := nextSymbol()
    else error(...)
}
-}

ziel :: [Token] -> [Token]
ziel [] = [Fehler ("Erhaltene Eingabe:\nErwartete Eingabe: \":-\"")]
ziel ((Fehler x):xs) = ((Fehler x):xs)
ziel tokens = if head tokens == Implikation then zielthen $ literal $ tail tokens else [Fehler ("Erhaltene Eingabe: " ++ fehlerUebersetzen (head tokens) ++ "\nErwartete Eingabe: \":-\"")]

zielthen :: [Token] -> [Token]
zielthen [] = [Fehler ("Erhaltene Eingabe:\nErwartete Eingaben: \",\" oder \".\"")]
zielthen ((Fehler x):xs) = ((Fehler x):xs)
zielthen ((Komma):tokens) = zielthen $ literal tokens
zielthen ((Punkt):tokens) = tokens
zielthen (t:tokens) = [Fehler ("Erhaltene Eingabe: " ++ fehlerUebersetzen t ++ "\nErwartete Eingaben: \",\" oder \".\"")]

{-
void Literal(){
    if (sym=="not") {sym:=nextSymbol}
    code fÃ¼r LTerm
}
-}

literal :: [Token] -> [Token]
literal [] = [Fehler ("Erhaltene Eingabe:\nErwartete Eingaben: \"not\", Variable mit belibigen Namen (die mit GroÃbuchstaben beginnt) oder NichtVariableLTerm (der mit Kleinbuchstaben beginnt)")]
literal ((Fehler x):xs) = ((Fehler x):xs)
literal (t:tokens) | t == Not || t == Name "" || t == Variable "" = if t == Not then literal tokens else lTerm (t:tokens)
literal (t:tokens) = [Fehler ("Erhaltene Eingabe: " ++ fehlerUebersetzen t ++ "\nErwartete Eingaben: \"not\", Variable mit belibigen Namen (die mit GroÃbuchstaben beginnt) oder NichtVariableLTerm (der mit Kleinbuchstaben beginnt)")]


{-
void NVLT(){
    if (sym == Name) {sym := nextSymbol()
    else error(...)}
    if(sym=="("){
    if (sym == "(") {sym := nextSymbol()
    else error(...)}
    code fÃ¼r LTerm
    while (sym==","){sym:=nextSymbol()
    code fÃ¼r LTerm}
    if (sym == ")") {sym := nextSymbol()
    else error(...)}
    } 
-}

nVLT :: [Token] -> [Token]
nVLT [] = [Fehler ("Erhaltene Eingabe:\nErwartete Eingabe: NichtVariableLTerm (der mit Kleinbuchstaben beginnt)")]
nVLT ((Fehler x):xs) = ((Fehler x):xs)
nVLT tokens = if head tokens == (Name "") then if head (tail tokens) == KlammerAuf then nVLTwhile $ lTerm $ tail $ tail tokens else tail tokens else [Fehler ("Erhaltene Eingabe: " ++ fehlerUebersetzen (head tokens) ++ "\nErwartete Eingabe: NichtVariableLTerm (der mit Kleinbuchstaben beginnt)")]

nVLTwhile :: [Token] -> [Token]
nVLTwhile [] = [Fehler ("Erhaltene Eingabe:\nErwartete Eingaben: \",\" oder \")\"")]
nVLTwhile ((Fehler x):xs) = ((Fehler x):xs)
nVLTwhile ((Komma):tokens) = nVLTwhile $ lTerm tokens
nVLTwhile ((KlammerZu):tokens) = tokens
nVLTwhile (t:_) = [Fehler ("Erhaltene Eingabe: " ++ fehlerUebersetzen t ++ "\nErwartete Eingaben: \",\" oder\")\"")]

{-
void LTerm(){
    case
    (sym==Variable){sym:=nextSymbol}
    (sym â first(NVLT)
    code fÃ¼r NVLT)
    else error(...)
}
-}

lTerm :: [Token] -> [Token]
lTerm [] = [Fehler ("Erhaltene Eingabe:\nErwartete Eingaben: Variable mit belibigen Namen (die mit GroÃbuchstaben beginnt) oder NichtVariableLTerm (der mit Kleinbuchstaben beginnt)")]
lTerm tokens = case tokens of
    ((Fehler x):xs)              -> ((Fehler x):xs)
    ((Variable _):xs)            -> xs
    (x:xs) | elem x (first NVLT) -> nVLT (x:xs)
    (x:_) | otherwise            -> [Fehler ("Erhaltene Eingabe: " ++ fehlerUebersetzen x ++ "\nErwartete Eingaben: Variable mit belibigen Namen (die mit GroÃbuchstaben beginnt) oder NichtVariableLTerm (der mit Kleinbuchstaben beginnt)")]


fehlerUebersetzen :: Token -> [Char]
fehlerUebersetzen KlammerAuf = "("
fehlerUebersetzen KlammerZu = ")"
fehlerUebersetzen Implikation = ":-"
fehlerUebersetzen Punkt = "."
fehlerUebersetzen Komma = ","
fehlerUebersetzen Not = "not"
fehlerUebersetzen (Variable string) = string
fehlerUebersetzen (Name string) = string
fehlerUebersetzen (Unknown string) = "Unknown: " ++ string
fehlerUebersetzen (Fehler string) = "Fehler: " ++ string