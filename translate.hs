module Translate where

import Data.List
import EigeneDatentypen


translate:: [Token] -> [MLBefehle]
translate [] = [Prompt]
translate (t:tokens)  | t == Implikation = [Push "BegEnv"] ++ (translateEnv tokens) ++ translateBody tokens
translate tokens = [Push "BegEnv"] ++ (translateEnv tokens) ++ translateHead tokens 


translateEnv :: [Token] -> [MLBefehle]
translateEnv tokens = hilfe tokens [] where
    hilfe (Punkt:tokens) _ = [Push "EndEnv"]
    hilfe ((Variable t):tokens) liste | not (elem t liste) = [Push ("VAR "++ t)] ++ hilfe tokens (t:liste)
    hilfe (_:tokens) liste = hilfe tokens liste

translateBody :: [Token] -> [MLBefehle]
translateBody (t1:t2:tokens) | t1 == Punkt = [Return "pos"] ++ translate (t2:tokens)
translateBody ((Not):(Name t):tokens) = [Push "not"] ++ pushT((Name t):tokens) ++ [Return "neg", Backtrack] ++ translateBody (tba tokens 0)
translateBody (t:tokens) | t == Punkt = translate tokens
translateBody ((Name t):tokens) = pushT((Name t):tokens) ++ translateBody (tba tokens 0)
translateBody (t:tokens) | t == Komma = translateBody tokens
translateBody (KlammerAuf:tokens) = translateBody tokens
translateBody (KlammerZu:tokens) = translateBody tokens

pushT :: [Token] -> [MLBefehle]
pushT tokens = [Push "CHP"] ++ hilfe tokens 0 where 
    hilfe (t:tokens) 0 | t == Punkt || t == Komma = [Push "EndAtom", Call, Backtrack]    
    hilfe ((Name t):tokens) zaehler = [Push ("STR " ++ t ++ " " ++ (show (arityT tokens)))] ++ hilfe tokens zaehler
    hilfe ((Variable t):tokens) zaehler = [Push ("VAR " ++ t)] ++ hilfe tokens zaehler
    hilfe (t:tokens) zaehler | t == KlammerZu = hilfe tokens (zaehler-1)
    hilfe (t:tokens) zaehler | t == KlammerAuf = hilfe tokens (zaehler + 1)
    hilfe (_:tokens) zaehler = hilfe tokens zaehler


translateHead :: [Token] -> [MLBefehle]
translateHead (t:tokens) | t == (Name "") = unifyT (t:tokens) ++ translateHead tokens
translateHead (t:tokens) | t == (Variable "") = unifyT (t:tokens) ++ translateHead tokens
translateHead (t:tokens) | t == Punkt = [Return "pos"] ++ translate tokens
translateHead (t:tokens) | t == Implikation = translateBody tokens
translateHead (_:tokens) = translateHead tokens

unifyT:: [Token] -> [MLBefehle]
unifyT ((Name a):as) = [Unify ("STR " ++ a ++ " " ++ (show (arityT as))), Backtrack]
unifyT ((Variable a):as) = [Unify ("VAR " ++ a), Backtrack]

arityT :: [Token] -> Int
arityT (t:tokens) | t == Komma || t == Implikation || t == Punkt || t == KlammerZu = 0
arityT (t:tokens) | t == KlammerAuf = hilfe tokens 1 where
    hilfe (t:tokens) arity | t == KlammerZu = arity    
    hilfe (t:tokens) arity | t == Komma = hilfe tokens (arity + 1)
    hilfe (t:tokens) arity | t == KlammerAuf = hilfe (abtrennen tokens 1) arity where
        abtrennen tokens 0 = tokens
        abtrennen (t:tokens) zahl | t == KlammerAuf = abtrennen tokens (zahl + 1)
        abtrennen (t:tokens) zahl | t == KlammerZu = abtrennen tokens (zahl - 1)
        abtrennen (_:tokens) zahl = abtrennen tokens zahl
    hilfe (_:tokens) arity = hilfe tokens arity

tba :: (Eq a, Num a) => [Token] -> a -> [Token]
tba (t:tokens) 0 | t == Punkt || t == Komma = (t:tokens)
tba (t:tokens) zaehler | t == KlammerZu = tba tokens (zaehler-1)
tba (t:tokens) zaehler | t == KlammerAuf = tba tokens (zaehler + 1)
tba (_:tokens) zaehler = tba tokens zaehler
