module Tokeniser where

--import Ast
--import Translate
import EigeneDatentypen


endeName :: [Char]
endeName = [' ', '\n', ':', '(', ')', '.', ',']


tokenisingfun2 :: [Char] -> [Char] -> [Token]
tokenisingfun2 [] [] = []
tokenisingfun2 [] b | b == "not" = [Not]
tokenisingfun2 [] b | isupper b = [Variable b]
tokenisingfun2 [] b | islower b = [Name b]
tokenisingfun2 (x:xs) b
 | x `elem` endeName && b == "not" = [Not] ++ tokenisingfun2 (x:xs) []
 | x `elem` endeName && isupper b = [Variable b] ++ tokenisingfun2 (x:xs) [] 
 | x `elem` endeName && islower b = [Name b] ++ tokenisingfun2 (x:xs) [] 
 | x == ':' = tokenisingfun2 xs [x]
 | x == '-' && b == ":"= [Implikation] ++ tokenisingfun2 xs []
 | b == ":" = [Unknown ":"] ++ tokenisingfun2 (x:xs) []
 | x == '(' = [KlammerAuf] ++ tokenisingfun2 xs b
 | x == ')' = [KlammerZu] ++ tokenisingfun2 xs b
 | x == ',' = [Komma] ++ tokenisingfun2 xs b
 | x == '.' = [Punkt] ++ tokenisingfun2 xs b
 | x == ' ' || x == '\n' = tokenisingfun2 xs b
 | (x `elem` lower || x `elem` upper) && b == [] = tokenisingfun2 xs [x]
 | b /= [] = tokenisingfun2 xs (b ++ [x])
 | otherwise = [Unknown [x]] ++ tokenisingfun2 xs b